package com.nobleapplications.examplemodule

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.rjr.apps.simpleviewpageradapter.SimpleViewPagerAdapter
import kotlinx.android.synthetic.main.activity_example.*

class ExampleActivity : AppCompatActivity() {

    private lateinit var adapter: SimpleViewPagerAdapter<ExampleFragment>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_example)

        val fragments = arrayOf(
            ExampleFragment.buildFragment("Fragment 1"),
            ExampleFragment.buildFragment("Fragment 2"),
            ExampleFragment.buildFragment("Fragment 3"),
            ExampleFragment.buildFragment("Fragment 4")
        )

        adapter = SimpleViewPagerAdapter.buildAndSet(fragments, supportFragmentManager, viewPager)
        tabLayout.setupWithViewPager(viewPager)
    }

}
