package com.nobleapplications.examplemodule

import android.os.Bundle
import android.os.Handler
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.rjr.apps.simpleviewpageradapter.SimpleViewPagerAdapter

class ExampleFragment : Fragment(), SimpleViewPagerAdapter.TitledFragment, SimpleViewPagerAdapter.WatchedFragment {

    override fun getTitle() = arguments?.getString(TITLE)

    override fun onFragmentActive() {
        Toast.makeText(context ?: return, getTitle() + " is showing!", Toast.LENGTH_SHORT).also { it.setGravity(Gravity.CENTER, 0, 0) }.show()
    }

    override fun onFragmentInactive() {
        Handler().postDelayed({
            Toast.makeText(context ?: return@postDelayed, getTitle() + " is hidden!", Toast.LENGTH_SHORT).show()
        }, 200)
    }

    companion object {
        private const val TITLE = "title"

        fun buildFragment(title: String): ExampleFragment {
            val args = Bundle().also { it.putString(TITLE, title) }
            return ExampleFragment().also { it.arguments = args }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_example, container, false)
    }

}