package com.rjr.apps.simpleviewpageradapter

/*
 * Copyright (C) 2019 RJR Apps
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager

class SimpleViewPagerAdapter<FragmentType: Fragment> private constructor(fragments: Array<FragmentType>, fragmentManager: FragmentManager) : FragmentStatePagerAdapter(fragmentManager) {

    private var fragmentList = fragments.toMutableList()

    companion object {

        /**
         * Fragments implementing [WatchedFragment] can detect when fragments are active/inactive
         * */
        @JvmStatic
        fun <FragmentType: Fragment> buildAndSet(fragments: Array<FragmentType>, fragmentManager: FragmentManager, viewPagerToSet: ViewPager): SimpleViewPagerAdapter<FragmentType> {
            val adapter = SimpleViewPagerAdapter(fragments, fragmentManager)

            val listener = object : ViewPager.OnPageChangeListener {
                private var lastPosition = 0
                private var currentPosition = 0

                override fun onPageScrollStateChanged(state: Int) {
                    if (state == ViewPager.SCROLL_STATE_IDLE && lastPosition != currentPosition) {
                        val previousFragment = adapter.getItem(lastPosition)
                        val selectedFragment = adapter.getItem(currentPosition)

                        fragmentManager.fragments.forEach { fragment ->
                            if (fragment is WatchedFragment) {
                                if (fragment == selectedFragment) {
                                    fragment.onFragmentActive()
                                } else if (fragment == previousFragment){
                                    fragment.onFragmentInactive()
                                }
                            }
                        }

                        lastPosition = currentPosition
                    }
                }

                override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {  }

                override fun onPageSelected(position: Int) {
                    currentPosition = position
                }
            }

            viewPagerToSet.clearOnPageChangeListeners()
            viewPagerToSet.addOnPageChangeListener(listener)
            viewPagerToSet.adapter = adapter
            return adapter
        }

    }

    override fun getItem(position: Int): Fragment = fragmentList[position]
    override fun getCount(): Int = fragmentList.size
    override fun getPageTitle(position: Int): CharSequence? = (fragmentList[position] as? TitledFragment)?.getTitle()

    override fun getItemPosition(anyObject: Any): Int {
        return if (fragmentList.contains(anyObject)) {
            fragmentList.indexOf(anyObject)
        } else {
            PagerAdapter.POSITION_NONE
        }
    }

    fun getFragments() = fragmentList.toList()
    fun <T: FragmentType> getFragments(fragmentClass: Class<T>): List<T> {
        val fragments = fragmentList.filter { it.javaClass == fragmentClass }
        return when {
            fragments.isNotEmpty() -> fragments as List<T>
            else -> listOf()
        }
    }

    fun getFragment(position: Int) = fragmentList[position]
    fun <T: FragmentType> getFragment(fragmentClass: Class<T>) = fragmentList.find { it.javaClass == fragmentClass } as T

    fun setFragments(fragments: Array<FragmentType>) {
        fragmentList = fragments.toMutableList()
        notifyDataSetChanged()
    }

    fun addFragment(fragment: FragmentType): SimpleViewPagerAdapter<FragmentType> {
        if (!fragmentList.contains(fragment)) {
            fragmentList.add(fragment)
        }

        notifyDataSetChanged()
        return this
    }

    fun removeFragment(position: Int): SimpleViewPagerAdapter<FragmentType> {
        fragmentList.remove(fragmentList[position])
        notifyDataSetChanged()
        return this
    }

    fun removeFragment(fragmentClass: Class<out FragmentType>): SimpleViewPagerAdapter<FragmentType> {
        val fragment = fragmentList.find { it.javaClass == fragmentClass } ?: return this
        fragmentList.remove(fragment)
        notifyDataSetChanged()
        return this
    }

    /**
     * To title your fragments, have them implement this interface
     * */
    interface TitledFragment {
        fun getTitle(): String?
    }

    /**
     * To receive callbacks on fragment active or inactive, have them implement this interface
     * */
    interface WatchedFragment {
        fun onFragmentActive()
        fun onFragmentInactive()
    }

}