#SimpleViewPagerAdapter

With `ViewPager` adapters, it's easy to get in trouble when trying to fetch a particular fragment. It's also difficult to track within fragments whether or not the fragment has been shown or has been hidden. `SimpleViewPagerAdapter` aims to solve these problems.

#Download

First, add the JitPack repository in your root build.gradle file at the end of repositories:
```
allprojects {
    repositories {
        ...
        maven { url 'https://jitpack.io' }
    }
}
```

Then add the dependency in your app's build.gradle file:
```
dependencies {
    implementation 'org.bitbucket.rjr-apps:simpleviewpageradapter:1.0.0-1'
}
```

#Basic Usage

All you have to do is call `SimpleViewPagerAdapter.buildAndSet()`; and pass in your fragments, the `FragmentManager`, and the `ViewPager`:
```
val adapter = SimpleViewPagerAdapter.buildAndSet(arrayOfFragments, supportFragmentManager, viewPager)
```

##Titled Fragments

Do your fragments have titles that you'd like to use with a `TabLayout` or other things? Just implement `SimpleViewPagerAdapter.TitledFragment` on the fragments you're using with it:
```
class ExampleFragment : Fragment(), SimpleViewPagerAdapter.TitledFragment {

    override fun getTitle() = "Fragment title"
    
}    
```

##Callbacks on Fragments Shown or Hidden

Would you like a callback in your fragment when it is shown on the screen, or is hidden from the screen? Just implement `SimpleViewPagerAdapter.WatchedFragment` on the fragments you need to track:
```
class ExampleFragment : Fragment(), SimpleViewPagerAdapter.WatchedFragment {

    override fun onFragmentActive() {
        // Do something when the fragment is shown
    }

    override fun onFragmentInactive() {
        // Do something when the fragment is hidden
    }
    
}    
```